# Cyber-Enquete_Sur la piste du hacker


Il faut imprimer les deux fichiers pdf des fiches pour pouvoir utiliser pleinement l'escape game : 
[kit des fiches Print&Play](https://forge.apps.education.fr/cyber/cyber-enquete/-/blob/main/public/EDUCNAT%20-%20ESCAPEGAME_PRINT&PLAY_v1.8.zip) 

L'application en ligne pour jouer :
https://cyber.forge.apps.education.fr/cyber-enquete/

Mélanie Fenaert a rédigé une belle présentation de l'outil sur le site de l'association S'Cape : https://scape.enepe.fr/cyber-enquete.html
